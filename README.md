# couchbase-images

This repository exists to hold a Dockerfile for two Couchbase provided services that are needed by the Manuscripts backend:

- Couchbase Server (5.5.4 at the time of writing).
- Couchbase Sync Gateway.

## Why?

- The version of Sync Gateway used in the Manuscripts stack is miniscule [fork](https://github.com/mpapp/sync_gateway/tree/feature/per-origin-cookie-domain-config) tracking the master branch of the upstream repository, with the following change applied on top of the upstream version: https://github.com/couchbase/sync_gateway/pull/3820. If this change were accepted, or an alternative to it found, upstream versions could be used without source changes.

- The reason there is a distinction between this repository (couchbase-images) and the Sync Gateway image published by [manuscripts-sync](https://gitlab.com/mpapp-public/manuscripts-sync) is the large build time taken up by building Sync Gateway, which a pre-built Sync Gateway build in the form of the Docker image published by this repository solves. Fast iterations on manuscripts-sync particulars are possible (altering its sync function implementation, the schema version referenced etc).