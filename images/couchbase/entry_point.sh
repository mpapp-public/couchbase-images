#!/bin/bash

set -m

NFS_MOUNTPOINT="/$NFS/$HOSTNAME"

if [[ -v "${NFS_MOUNTPOINT}" ]]; then
mkdir -p "${NFS_MOUNTPOINT}" && chown couchbase:couchbase "${NFS_MOUNTPOINT}"
fi

/entrypoint.sh couchbase-server &

attempts=0

while ! timeout 1 bash -c "echo > /dev/tcp/localhost/8091" 2>/dev/null
do
  attempts=$((attempts + 1))
  if [[ $attempts -eq 100 ]]
  then
    echo "Failed to connect to Couchbase Server within timeout."
    exit 1
  fi
  sleep 1
done

set -e # failing commands can happen before this point.

export IP=`hostname -I|tr -d ' '`

echo "Couchbase visible at ${IP}. Now initialising…"

# couchbase-cli node-init -c ${IP}:8091 \
#  -u ${APP_COUCHBASE_ADMIN_USER} \
#  -p ${APP_COUCHBASE_ADMIN_PASS} \
#  --node-init-hostname=$IP
#
# we are not building a multi-node cluster.

echo "\nCreating pool..."
curl -s -XPOST http://127.0.0.1:8091/pools/default -u ${APP_COUCHBASE_ADMIN_USER}:${APP_COUCHBASE_ADMIN_PASS} -d memoryQuota=${CLUSTER_RAM_SIZE_MB} -d indexMemoryQuota=${INDEX_RAM_SIZE_MB}

echo "\nCreating services..."
curl -s -XPOST http://127.0.0.1:8091/node/controller/setupServices -d services=kv%2Cn1ql%2Cindex%2Cfts%2Ceventing

echo "\nSetting username and password..."
curl -s -XPOST http://127.0.0.1:8091/settings/web -d port=8091 -d username=${APP_COUCHBASE_ADMIN_USER} -d password=${APP_COUCHBASE_ADMIN_PASS}

echo "\nSetting index service settings..."
curl -s -XPOST http://127.0.0.1:8091/settings/indexes -u ${APP_COUCHBASE_ADMIN_USER}:${APP_COUCHBASE_ADMIN_PASS} -d 'storageMode=memory_optimized'

if [[ $APP_INDEX_REPLICA_COUNT -eq 0 ]]; then
  ENABLE_INDEX_REPLICA=0
else
  ENABLE_INDEX_REPLICA=1
fi

if [[ $COUCHBASE_MASTER_SERVER ]]; then
  # TODO: Add N retries here.
  curl -o /dev/null -s --head --fail \
  "http://${COUCHBASE_MASTER_SERVER}:8091/pools/default/buckets/${APP_DATA_BUCKET}" \
  -u "${APP_DATA_BUCKET}:${APP_COUCHBASE_RBAC_PASSWORD}"

  if [ $? -eq 0 ]; then
    echo "Cluster exists – adding node to cluster…"
    couchbase-cli server-add -c ${COUCHBASE_MASTER_SERVER}:8091 \
    -u ${APP_COUCHBASE_ADMIN_USER} -p ${APP_COUCHBASE_ADMIN_PASS} \
    --server-add ${IP}:8091 \
    --server-add-username ${APP_COUCHBASE_ADMIN_USER} \
    --server-add-password ${APP_COUCHBASE_ADMIN_PASS} \
    --services kv,n1ql,index,fts,eventing
  fi
fi

if [[ $COUCHBASE_INITIALIZE == 'true' ]]; then
  declare -A bucket_ramsizes
  bucket_ramsizes=(\
  [$APP_DATA_BUCKET]='256' \
  [$APP_USER_BUCKET]='128' \
  [$APP_STATE_BUCKET]='128' \
  [$APP_DERIVED_DATA_BUCKET]='128' \
  [$APP_DISCUSSIONS_BUCKET]='128')

  for bucket in "${!bucket_ramsizes[@]}"; do
    echo "Creating ${bucket} bucket with RAM size ${bucket_ramsizes[${bucket}]}M (index replicas enabled: ${ENABLE_INDEX_REPLICA})..."
    couchbase-cli bucket-create -c http://127.0.0.1:8091 \
    -u ${APP_COUCHBASE_ADMIN_USER} \
    -p ${APP_COUCHBASE_ADMIN_PASS} \
    --bucket=${bucket} \
    --bucket-type=couchbase \
    --bucket-ramsize=${bucket_ramsizes[${bucket}]} \
    --enable-index-replica=${ENABLE_INDEX_REPLICA}

    echo "Creating RBAC user for ${bucket} (for sync gateway)..."
    couchbase-cli user-manage -c http://127.0.0.1:8091 \
    -u ${APP_COUCHBASE_ADMIN_USER} \
    -p ${APP_COUCHBASE_ADMIN_PASS} \
    --set \
    --rbac-username "${bucket}" \
    --rbac-password "${APP_COUCHBASE_RBAC_PASSWORD}" \
    --roles bucket_full_access[${bucket}] \
    --auth-domain local
  done
  echo "Buckets created."
fi

fg 1
